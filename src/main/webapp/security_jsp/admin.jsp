<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!-- Spring Security提供的taglib -->
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE>
<html>
<head>
<title>管理员界面</title>
</head>
<body>
    欢迎来到管理员页面.
    <br />
    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
        <a href="${pageContext.request.contextPath}/j_spring_security_logout">注销</a>
    </sec:authorize>
    <br>
</body>
</html>