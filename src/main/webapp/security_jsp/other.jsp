<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="sec"
    uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>其他游客访问页面</title>
</head>
<body>
    其他游客访问页面.
    <br />
    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
        <a href="${pageContext.request.contextPath}/j_spring_security_logout">注销</a>
    </sec:authorize>
</body>
</html>