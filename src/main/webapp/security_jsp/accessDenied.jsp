<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!-- Spring Security提供的taglib -->
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE>
<html>
<head>
<title>访问拒绝</title>
</head>
<body>
    您的访问被拒绝，无权访问该资源！只有会员和管理员才能访问，你是游客角色。
    <br />
    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
        <a href="${pageContext.request.contextPath}/j_spring_security_logout">注销</a>
    </sec:authorize>
    <br>
</body>
</html>