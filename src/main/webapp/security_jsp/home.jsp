<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

<!-- Spring Security提供的taglib -->
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>首页</title>
</head>
<body>
    这是首页，欢迎
    <sec:authentication property="name" />
    !
    <br> 您拥有的权限：
    <sec:authentication property="authorities" var="authorities"
        scope="page" />
    <c:forEach items="${authorities}" var="authority">
  		${authority.authority}
	</c:forEach>
    <br />

    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
        <a
            href="${pageContext.request.contextPath}/j_spring_security_logout">注销</a>
    </sec:authorize>

    <!-- 只有满足拥有admin权限才显示 .里面也可以指定多个角色，逗号分隔-->
    <sec:authorize ifAllGranted="ROLE_ADMIN">
        <a href="admin.jsp">进入admin页面</a>
    </sec:authorize>

    <!-- 只要拥有ROLE_ADMIN或者ROLE_USER中的其中一个角色，即可显示 -->
    <sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_USER">
        <a href="other.jsp">进入其它页面</a>
    </sec:authorize>


    <!-- 
		知识点补充：
			为不同用户显示各自的登陆成功页面————
			一个常见的需求是，普通用户登录之后显示普通用户的工作台，管理员登陆之后显示后台管理页面。这个功能可以使用taglib解决。
			其实只要在登录成功后的jsp页面中使用taglib判断当前用户拥有的权限进行跳转就可以。
			
			<sec:authorize ifAllGranted="ROLE_ADMIN">1
  				< % response.sendRedirect("admin.jsp"); %>
			</sec:authorize>
			<sec:authorize ifNotGranted="ROLE_ADMIN">2
  				< % response.sendRedirect("user.jsp");%>
			</sec:authorize>
			
			当用户拥有ROLE_ADMIN权限时，既跳转到admin.jsp
			当用户没有ROLE_ADMIN权限时，既跳转到user.jsp
	 -->
</body>
</html>