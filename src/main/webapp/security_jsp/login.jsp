<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://"
            + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE>
<html>
<head>
<title>SpringSecurity认证登录</title>
</head>
<body>
    <p>
        请使用以下数据库中存在的用户名和密码登录测试：<br />
    </p>
    <p>zb1 123456 ——拥有管理员、游客、会员3个角色</p>
    <p>zb2 123456 ——拥有游客、会员2个角色</p>
    <p>zb3 123456 ——拥有游客1个角色</p>
    <p>zb4 123456 ——拥有游客1个角色</p>
    <p>zhoubang 123456 ——拥有游客、会员2个角色</p>
    <br />
    <br />
    <br />
    <span style="color: red;">${sessionScope.SPRING_SECURITY_LAST_EXCEPTION}</span>
    <span style="color: red;">${sessionScope.loginErrMsg}</span>
    <!-- 
			<span>${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}</span>
		 -->
    <form
        action="${pageContext.request.contextPath}/j_spring_security_check"
        method="POST">
        <table>
            <tr>
                <td>用户:</td>
                <td><input type='text' name='j_username'></td>
            </tr>
            <tr>
                <td>密码:</td>
                <td><input type='password' name='j_password'></td>
            </tr>
            <tr>
                <td>两周内自动登录:</td>
                <td><input id="_spring_security_remember_me"
                    name="_spring_security_remember_me" type="checkbox"
                    value="true" /></td>
            </tr>
            <%-- <tr>
        			<td>验证码:</td>
        			<td>
        				<input type="text" name="j_captcha" id="j_captcha"/>
        				<img id="codeImg" alt="" src="<%=basePath%>ImageServlet" height="30px" width="70px">
        				<a href="javascript:void(0)" onclick="changeCodeImg();">点击换一张</a>
        			</td>
        		</tr> --%>
            <tr>
                <td><input name="reset" type="reset"></td>
                <td><input name="submit" type="submit"></td>
            </tr>
        </table>
    </form>
</body>
</html>
