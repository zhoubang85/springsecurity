package com.zb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zb.dao.SecurityDao;
import com.zb.security.pojo.Sec_User;
import com.zb.service.SecurityService;

@Service(value = "securityServiceImpl")
@Transactional
public class SecurityServiceImpl implements SecurityService {

    @Resource(name = "securityDaoImpl")
    private SecurityDao securityDao;

    @Override
    public Sec_User getUserByAccount(String userAccount) {
        Sec_User user = securityDao.getUserByAccount(userAccount);
        return user;
    }

    @Override
    public String getUserRolesByAccount(String userAccount) {
        String roles = securityDao.getUserRolesByAccount(userAccount);
        return roles;
    }

    @Override
    public List<Map<String, String>> getResourceAndThisRoles() {
        return securityDao.getResourceAndThisRoles();
    }

    @Override
    public int register(Sec_User sec_User) {
        int id = securityDao.register(sec_User);
        int count = 0;
        if (id > 0) {
            List<Integer> roles = new ArrayList<>();
            roles.add(2);
            roles.add(3);
            count = securityDao.updateUserRole(id, roles);
        }
        return count;
    }

    @Override
    public boolean checkAccount(String account) {
        return securityDao.checkAccount(account);
    }

    @Override
    public Sec_User login(String account, String password) {
        Sec_User sec_User = securityDao.login(account, password);
        return sec_User;
    }

    @Override
    public List<Map<String, String>> getAllSecUser() {
        List<Map<String, String>> userList = securityDao.getAllSecUser();
        return userList;
    }
}
