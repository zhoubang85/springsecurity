package com.zb.service;

import java.util.List;
import java.util.Map;

import com.zb.security.pojo.Sec_User;

/**
 * 权限认证service
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:37:10
 */
public interface SecurityService {

    /**
     * 根据用户名获取用户
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午1:05:22
     * @param userName
     * @return
     */
    public Sec_User getUserByAccount(String userAccount);

    /**
     * 根据登录用户名获取用户拥有的权限列表
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午1:45:31
     * @param userAccount
     * @return
     */
    public String getUserRolesByAccount(String userAccount);

    /**
     * 获取每一个资源和该资源对应的角色列表
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午2:18:24
     * @return
     */
    public List<Map<String, String>> getResourceAndThisRoles();

    /**
     * 注册用户
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午1:34:07
     * @param sec_User
     * @return
     */
    public int register(Sec_User sec_User);

    /**
     * 检查账户名称是否存在
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午3:06:57
     * @param account
     * @return
     */
    public boolean checkAccount(String account);

    /**
     * 登录
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午4:54:34
     * @param account
     * @param password
     * @return
     */
    public Sec_User login(String account, String password);

    /**
     * 获取所有SecUser
     * 
     * @author zhoubang 创建时间 ： 2014年12月18日 下午2:45:23
     * @return
     */
    public List<Map<String, String>> getAllSecUser();
}
