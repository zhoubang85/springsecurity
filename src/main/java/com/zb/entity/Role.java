package com.zb.entity;

import java.io.Serializable;

/**
 * 角色实体类
 * 
 * 作者: zhoubang 日期：2015年8月25日 下午5:33:31
 */
public class Role implements Serializable {

    private static final long serialVersionUID = 5386278312371202995L;

    private int id;
    private String roleName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
