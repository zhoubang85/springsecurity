package com.zb.entity;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:33:42
 */
public class User implements Serializable{
	private static final long serialVersionUID = 4321178667528474490L;
	
	private int id;
	private String name;
	private String pwd;
	private String sex;
	private int age;
	private String phone;
	private Set<Role> roleIds;//角色列表
	
	
	
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Set<Role> getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(Set<Role> roleIds) {
		this.roleIds = roleIds;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public User() {
		
	}
	
	
	
	public User(int id, String name, String pwd, String sex, int age,
			String phone, Set<Role> roleIds) {
		super();
		this.id = id;
		this.name = name;
		this.pwd = pwd;
		this.sex = sex;
		this.age = age;
		this.phone = phone;
		this.roleIds = roleIds;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", pwd=" + pwd + ", sex="
				+ sex + ", age=" + age + ", phone=" + phone + ", roleIds="
				+ roleIds + "]";
	}
	
}
