package com.zb.dao;

import java.util.List;
import java.util.Map;

import com.zb.security.pojo.Sec_User;

/**
 * 
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:33:21
 */
public interface SecurityDao {

    /**
     * 根据用户名获取用户
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午1:05:58
     * @param userAccount
     * @return
     */
    public Sec_User getUserByAccount(String userAccount);

    /**
     * 根据登录用户名获取用户拥有的权限列表
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午1:44:47
     * @param userAccount
     * @return
     */
    public String getUserRolesByAccount(String userAccount);

    /**
     * 获取每一个资源和该资源对应的角色列表
     * 
     * @author zhoubang 创建时间 ： 2014年11月28日 下午2:17:53
     * @return
     */
    public List<Map<String, String>> getResourceAndThisRoles();

    /**
     * 注册用户
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午1:38:18
     * @param sec_User
     * @return
     */
    public int register(Sec_User sec_User);

    /**
     * 更新用户角色
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午2:01:10
     * @param userId
     * @param roleId
     * @return
     */
    public int updateUserRole(int id, List<Integer> roles);

    /**
     * 检查账户名称是否存在
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午3:06:15
     * @param account
     * @return
     */
    public boolean checkAccount(String account);

    /**
     * 登录
     * 
     * @author zhoubang 创建时间 ： 2014年12月10日 下午4:54:51
     * @param account
     * @param password
     * @return
     */
    public Sec_User login(String account, String password);

    /**
     * 查询所有SecUser数据
     * 
     * @author zhoubang 创建时间 ： 2014年12月18日 下午2:47:44
     * @return
     */
    public List<Map<String, String>> getAllSecUser();

}
