package com.zb.security.pojo;

import java.io.Serializable;

/**
 * 角色资源表
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:35:15
 */
public class RoleResource implements Serializable {

    private static final long serialVersionUID = -8163459760472002409L;

    private int id;
    private int roleId;
    private int resourceId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

}
