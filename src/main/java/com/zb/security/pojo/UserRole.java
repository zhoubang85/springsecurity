package com.zb.security.pojo;

import java.io.Serializable;

/**
 * 用户角色表
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:35:39
 */
public class UserRole implements Serializable {

    private static final long serialVersionUID = 119379714197952275L;

    private int id;
    private int userId;
    private int roleId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

}
