package com.zb.security.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户表
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:35:26
 */
public class Sec_User implements Serializable {
    private static final long serialVersionUID = 507295582180945766L;

    private int id;
    private String account;// 登录账户名
    private String userName;// 真实用户名
    private String password;// 登录密码
    private int enable;// 账户是否可用 1:可用 -1:不可用
    private int issys;// 是否是系统管理员 -1:不是 1:是
    private int expiredStatus;// 账户是否过期 -1:是 1:否
    private int lockStatus;// 账户是否被锁住 -1:是 1:否
    private int credentialExpired;// 账户凭证是否过期 1:是 -1:否

    private String email;// 邮箱
    private Date createTime;// 注册时间
    private Date updateTime;// 最后更新时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    public int getIssys() {
        return issys;
    }

    public void setIssys(int issys) {
        this.issys = issys;
    }

    public int getExpiredStatus() {
        return expiredStatus;
    }

    public void setExpiredStatus(int expiredStatus) {
        this.expiredStatus = expiredStatus;
    }

    public int getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(int lockStatus) {
        this.lockStatus = lockStatus;
    }

    public int getCredentialExpired() {
        return credentialExpired;
    }

    public void setCredentialExpired(int credentialExpired) {
        this.credentialExpired = credentialExpired;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Sec_User [id=" + id + ", account=" + account + ", userName="
                + userName + ", password=" + password + ", enable=" + enable
                + ", issys=" + issys + ", expiredStatus=" + expiredStatus
                + ", lockStatus=" + lockStatus + ", credentialExpired="
                + credentialExpired + ", email=" + email + ", createTime="
                + createTime + ", updateTime=" + updateTime + "]";
    }
}
