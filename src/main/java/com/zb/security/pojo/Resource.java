package com.zb.security.pojo;

import java.io.Serializable;

/**
 * 资源表
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:34:53
 */
public class Resource implements Serializable {

    private static final long serialVersionUID = -8963027680145675744L;

    private int id;
    private String resourceName;// 资源名称
    private String resourceUrl;// 资源路径
    private int enable;// 资源是否可以访问，0为不可用，1为可用。

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public int getEnable() {
        return enable;
    }

    public void setEnable(int enable) {
        this.enable = enable;
    }

    @Override
    public String toString() {
        return "Resource [id=" + id + ", resourceName=" + resourceName
                + ", resourceUrl=" + resourceUrl + ", enable=" + enable + "]";
    }

}
