package com.zb.security.pojo;

import java.io.Serializable;

/**
 * 角色表
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:35:05
 */
public class Role implements Serializable {

    private static final long serialVersionUID = -7653805450361355061L;

    private int id;
    private String roleName;
    private int enabled;
    private String roleOrder;
    private String roleNote;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public String getRoleOrder() {
        return roleOrder;
    }

    public void setRoleOrder(String roleOrder) {
        this.roleOrder = roleOrder;
    }

    public String getRoleNote() {
        return roleNote;
    }

    public void setRoleNote(String roleNote) {
        this.roleNote = roleNote;
    }

    @Override
    public String toString() {
        return "Role [id=" + id + ", roleName=" + roleName + ", enabled="
                + enabled + ", roleOrder=" + roleOrder + ", roleNote="
                + roleNote + "]";
    }

}
