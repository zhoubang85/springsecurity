package com.zb.security.listener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.zb.service.SecurityService;

/**
 * web容器启动，监听获取 资源与角色 的映射
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:34:42
 */
public class ResourcesRolesListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce = null;
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContext applicationContext = WebApplicationContextUtils
                .getWebApplicationContext(sce.getServletContext());
        SecurityService securityService = (SecurityService) applicationContext
                .getBean("securityServiceImpl");

        // 数据库读取资源与角色映射关系.返回的是列表.dao层接收结果采用的是List<Map<key,valye>>的形式
        List<Map<String, String>> resRoles = securityService
                .getResourceAndThisRoles();
        if (null != resRoles && resRoles.size() > 0) {
            // 存放资源与角色映射的结果,并存入上下文
            Map<String, String> resRolesMap = new HashMap<String, String>();
            // 获取每一个资源与对应的角色.key为资源的URL，value为该URL必须的角色列表(以","分隔的字符串)
            for (Map<String, String> map : resRoles) {
                // URL、ROLES是在sql中对结果列进行的命名名称，
                resRolesMap.put(map.get("URL"), map.get("ROLES"));
            }
            // 将资源与角色映射存入上下文
            sce.getServletContext().setAttribute("resRolesMap", resRolesMap);
        }
    }
}
