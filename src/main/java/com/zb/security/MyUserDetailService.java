package com.zb.security;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.Resource;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.zb.security.pojo.Sec_User;
import com.zb.service.SecurityService;

/**
 * 从数据库中读入用户的密码，角色信息，是否锁定，账号是否过期等
 * 
 * 作者: zhoubang 
 * 日期：2015年8月25日 下午5:36:58
 */
@SuppressWarnings("deprecation")
public class MyUserDetailService implements UserDetailsService {

    @Resource(name = "securityServiceImpl")
    private SecurityService securityService;

    // 下面注释的代码，是测试使用。使用硬编码进行权限设置的。
    /*
     * @Override public UserDetails loadUserByUsername(String userName) throws
     * UsernameNotFoundException, DataAccessException {
     * 
     * Collection<GrantedAuthority> auths=new ArrayList<GrantedAuthority>();
     * GrantedAuthorityImpl auth2=new GrantedAuthorityImpl("ROLE_USER");
     * auths.add(auth2); if(userName.equals("zhoubang")){ //为用户新增角色
     * GrantedAuthorityImpl auth1=new GrantedAuthorityImpl("ROLE_ADMIN");
     * auths.add(auth1); }
     * 
     * **************************************************************************
     * ************************************************ User实体的参数. User(String
     * username, String password, boolean enabled, boolean accountNonExpired,
     * boolean credentialsNonExpired, boolean accountNonLocked, Collection<?
     * extends GrantedAuthority> authorities)
     * 
     * 如果spring-security.xml中配置了密码采用md5加密，即：<password-encoder hash="md5" />
     * 则，此处User的password参数必须是经过md5加密之后的值
     * ，比如：前端用户输入登录的密码为123456,则这里需要传入e10adc3949ba59abbe56e057f20f883e
     * ************
     * **************************************************************
     * ************************************************ User user = new
     * User(userName,"123456", true, true, true, true, auths); return user;
     * 
     * }
     */

    /**
     * 数据库交互获取用户拥有的权限角色，并设置权限
     */
    @Override
    public UserDetails loadUserByUsername(String userAccount)
            throws UsernameNotFoundException, DataAccessException {
        // 根据登录用户名获取用户信息
        Sec_User user = securityService.getUserByAccount(userAccount);
        if (null != user) {
            // 存放权限
            Collection<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();

            // 获取该用户拥有的权限
            String roles = securityService.getUserRolesByAccount(userAccount);
            if (null != roles && !"".equals(roles.trim())) {
                String[] role = roles.split(",");
                for (int i = 0; i < role.length; i++) {
                    GrantedAuthorityImpl auth = new GrantedAuthorityImpl(
                            role[i]);
                    auths.add(auth);
                }
            }

            User userDetails = new User(userAccount, user.getPassword(),
                    user.getEnable() != -1 ? true : false,
                    user.getExpiredStatus() != -1 ? true : false,
                    user.getCredentialExpired() != -1 ? true : false,
                    user.getLockStatus() != -1 ? true : false, auths);
            return userDetails;
        }
        return null;
    }
}
